<?php

namespace Pkgfigueira\Domain\Entities\Models;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbsiaf.aluno';
    /**
     * @var string
     */
    protected $primaryKey = 'cod_aluno';
    /**
     * @var array
     */
    protected $hidden = ['cod_usuario_log', 'dat_operacao_log'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
